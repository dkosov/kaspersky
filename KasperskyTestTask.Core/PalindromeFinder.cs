﻿using System;
using System.IO;

namespace KasperskyTestTask.Core
{
    public static class PalindromeFinder
    {
        /// <summary>
        /// Возвращает подстроку полиндром в заданном текстовом файле.
        /// Используется алгритм манакера.
        /// </summary>
        /// <param name="fileName">Имя текстового файла, содержащего исходную строку.</param>
        /// <returns>Наибольший полиндром.</returns>
        public static string FindInFile(string fileName)
        {
            if(string.IsNullOrEmpty(fileName))
                throw new ArgumentException("Не указано имя входного файла");

            if (Directory.Exists(fileName))
                throw new FileNotFoundException($"Такого файла не существует: {fileName}");

            var readedString = "";

            using (var streamReader = new StreamReader(fileName))
            {
                readedString = streamReader.ReadToEnd();
            }

            return FindInString(readedString);
        }

        /// <summary>
        /// Возвращает подстроку полиндром в исходной строке.
        /// </summary>
        /// <param name="wholeString">Исходная строка.</param>
        /// <returns>Наибольший полиндром.</returns>
        public static string FindInString(string wholeString)
        {
            if (string.IsNullOrEmpty(wholeString))
                throw new NullReferenceException("Пустая строка");

            // Новая строка с разделителями.
            var s2 = AddBoundaries(wholeString.ToCharArray());
            var p = new int[s2.Length];

            // Первый элемент в новой строке.
            int center = 0;

            // Радиус полином - расстояние от центра до крайнего элемента.
            int radius = 0; 

            // Индексы зеркальных элементов.
            int m = 0;
            int n = 0;

            // длина полиндрома.
            int length = 0;

            for (var i = 1; i < s2.Length; i++)
            {
                if (i > radius)
                {
                    p[i] = 0;
                    m = i - 1;
                    n = i + 1;
                }
                else
                {
                    int i2 = center * 2 - i;
                    if (p[i2] < (radius - i))
                    {
                        p[i] = p[i2];
                        m = -1;
                    }
                    else
                    {
                        p[i] = radius - i;
                        n = radius + 1;
                        m = i * 2 - n;
                    }
                }
                while (m >= 0 && n < s2.Length && s2[m] == s2[n])
                {
                    p[i]++;
                    m--;
                    n++;
                }

                if (i + p[i] > radius)
                {
                    center = i;
                    radius = i + p[i];
                }
            }

            center = 0;

            for (var i = 1; i < s2.Length; i++)
            {
                if (length >= p[i]) continue;

                length = p[i];
                center = i;
            }

            return new string(RemoveBoundaries(Copy(s2, center - length, center + length + 1)));
        }

        public static T[] Copy<T>(T[] source, int start, int end)
        {
            if(source == null)
                throw new ArgumentNullException("source");

            if(end <= start)
                throw new ArgumentOutOfRangeException("end", "индекс конца не должен быть меньше или равен индексу начала");

            if(end > source.Length || end < 0)
                throw new ArgumentOutOfRangeException("end", "Индекса конца не должен лежать за пределами исходного массива");

            if (start > source.Length || start < 0)
                throw new ArgumentOutOfRangeException("start", "Индекса начала не должен лежать за пределами исходного массива");

            int len = end - start;
            var destination = new T[len];

            for (int i = 0; i < len; i++)
            {
                destination[i] = source[start + i];
            }
            return destination;
        }

        private static char[] AddBoundaries(char[] cs)
        {
            if (cs == null || cs.Length == 0)
                return "**".ToCharArray();

            var cs2 = new char[cs.Length * 2 + 1];
            for (int i = 0; i < (cs2.Length - 1); i = i + 2)
            {
                cs2[i] = '*';
                cs2[i + 1] = cs[i / 2];
            }
            cs2[cs2.Length - 1] = '*';
            return cs2;
        }

        private static char[] RemoveBoundaries(char[] cs)
        {
            if (cs == null || cs.Length < 3)
                return "".ToCharArray();

            var cs2 = new char[(cs.Length - 1) / 2];
            for (int i = 0; i < cs2.Length; i++)
            {
                cs2[i] = cs[i * 2 + 1];
            }
            return cs2;
        }
    }
}
