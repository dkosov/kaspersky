﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace KasperskyTestTask.Core
{
    public class BlockingQueue<T> where T:class
    {
        private readonly Queue<T> _queue = new Queue<T>();
        private bool _isDisabled;


        public void Put(T obj)
        {
            lock (_queue)
            {
                _queue.Enqueue(obj);
                Monitor.Pulse(_queue);
            }
        }

        public void Disable()
        {
            lock (_queue)
            {
                _isDisabled = true;
                Monitor.PulseAll(_queue);
            }
        }

        public T Get()
        {
            T obj;
            lock (_queue)
            {
                if (_isDisabled)
                    return null;

                while (_queue.Count == 0)
                {
                    // не дадим ожидать вечно.
                    if (!Monitor.Wait(_queue, TimeSpan.FromMilliseconds(20000)))
                        return null;
                }

                obj = _queue.Dequeue();
                Monitor.Pulse(_queue);
            }
            return obj;
        }
    }
}
