﻿using System.Threading;
using KasperskyTestTask.Core;

namespace Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.Write(PalindromeFinder.FindInFile("input.txt"));
            System.Console.ReadKey();

            // 1. Пометим текущий поток, в нем Put будет ждать пока в очереди не появится объект.
            Thread.CurrentThread.Name = "MainThread";
            
            // 2. В этом же потоке создадим блокрующую очередь.
            var queue = new BlockingQueue<object>();

            // 3. В отдельном потоке положим объект в очередь и пометим этот поток.
            var thread = new Thread(() =>
            {
                System.Console.WriteLine($"Засыпаем в потоке: {Thread.CurrentThread.Name}");
                Thread.Sleep(10000);
                queue.Put(1);
            })
            {
                Name = "PuttingThread"
            };
            thread.Start();

            System.Console.WriteLine($"Возвращаем из потока: {Thread.CurrentThread.Name}");

            // Будет ждать Thread.Sleep(10000) пока не появится объект в очереди.
            var a = queue.Get();
            System.Console.WriteLine(a);
            System.Console.ReadKey();
        }

    }
}
