﻿using System.Threading;
using KasperskyTestTask.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KasperskyTestTask.Tests
{
    [TestClass]
    public class BlockingQueueTests
    {
        [TestMethod]
        public void GetWithPutingTest()
        {
            var queue = new BlockingQueue<object>();
            var thread = new Thread(() =>
            {
                Thread.Sleep(10000);
                queue.Put(1);
            });
            thread.Start();

            Assert.AreEqual(1, queue.Get());
        }

        [TestMethod]
        public void GetWithTimeoutTest()
        {
            var queue = new BlockingQueue<object>();
            var thread = new Thread(() =>
            {
                Thread.Sleep(22000);
                queue.Put(1);
            });
            thread.Start();
            var returnedValue = queue.Get();

            Assert.AreEqual(null, returnedValue);
        }

        [TestMethod]
        public void DisablingTest()
        {
            var queue = new BlockingQueue<object>();
            queue.Disable();
            Assert.AreEqual(null, queue.Get());
        }
    }
}
