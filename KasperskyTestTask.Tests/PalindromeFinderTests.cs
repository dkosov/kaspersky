﻿using System;
using System.IO;
using KasperskyTestTask.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KasperskyTestTask.Tests
{
    [TestClass]
    public class PalindromeFinderTests
    {
        [TestMethod]
        public void FileNotFoundTest()
        {
            Assert.ThrowsException<FileNotFoundException>(()=>PalindromeFinder.FindInFile("input1.txt"));
        }

        [TestMethod]
        public void FindInFileTest()
        {
            Assert.AreEqual(PalindromeFinder.FindInFile("input.txt"), "arozaupalanalapuazora");
        }

        [TestMethod]
        public void EmptyFileNameTest()
        {
            Assert.ThrowsException<ArgumentException>(() => PalindromeFinder.FindInFile(""));
        }

        [TestMethod]
        public void NullFileNameTest()
        {
            Assert.ThrowsException<ArgumentException>(() => PalindromeFinder.FindInFile(null));
        }

        [TestMethod]
        public void NullOriginalStringTest()
        {
            Assert.ThrowsException<NullReferenceException>(() => PalindromeFinder.FindInString(null));
        }

        [TestMethod]
        public void EmptyOriginalStringTest()
        {
            Assert.ThrowsException<NullReferenceException>(() => PalindromeFinder.FindInString(""));
        }

        [TestMethod]
        public void AlreadyFindedPol1()
        {
            Assert.AreEqual(PalindromeFinder.FindInString("sometextarozaupalanalapuazorasomeanothertext"), "arozaupalanalapuazora");
        }

        [TestMethod]
        public void AlreadyFindedPol2()
        {
            Assert.AreEqual(PalindromeFinder.FindInString("a"), "a");
        }

        [TestMethod]
        public void AlreadeFindedPol3()
        {
            // Тут несколько одинаковых, должен взять первое.
            Assert.AreEqual(PalindromeFinder.FindInString("anama"), "ana");
        }

        [TestMethod]
        public void AlreadyFindedPol4()
        {
            // Тут несколько одинаковых, должен взять первое.
            Assert.AreEqual(PalindromeFinder.FindInString("abaccddccefe"), "ccddcc");
        }

        [TestMethod]
        public void AlreadyFindedPol5()
        {
            // Тут несколько одинаковых, должен взять первое.
            Assert.AreEqual(PalindromeFinder.FindInString("anama"), "ana");
        }

        [TestMethod]
        public void CopyTest()
        {
            var numbers = new int[] {1, 2, 3, 4};
            var newArray = KasperskyTestTask.Core.PalindromeFinder.Copy(numbers, 1, 3);
            CollectionAssert.AreEqual(new int[] { 2, 3 }, newArray);
        }

        [TestMethod]
        public void CopyArgumentOutOfRangeTest()
        {
            var numbers = new int[] { 1, 2, 3, 4 };
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => Core.PalindromeFinder.Copy(numbers, 1, 1));
        }

        [TestMethod]
        public void CopyEndOutOfRangeTest()
        {
            var numbers = new int[] { 1, 2, 3, 4 };
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => Core.PalindromeFinder.Copy(numbers, 1, 5));
        }

        [TestMethod]
        public void CopyStartOutOfRangeTest()
        {
            var numbers = new int[] { 1, 2, 3, 4 };
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => Core.PalindromeFinder.Copy(numbers, 5, 1));
        }
    }
}
